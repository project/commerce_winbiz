<?php

/**
 * @file
 * Hooks provided by the Commerce WinBIZ module.
 */

/**
 * Alter the order export.
 *
 * This hook is called for every export data row.
 *
 * For a full list of field mappings and their indexes please refer
 * to the WinBIZ manual PDF.
 *
 * @param array $row_data
 * @param object $order
 */
function hook_commerce_winbiz_export_row_alter(&$row_data, $order) {
  // Override some customer information data.
  $row_data[22] = 'Company';
  $row_data[23] = 'Lastname';
  $row_data[24] = 'Firstname';
  $row_data[25] = 'Address';
  $row_data[26] = 'Premise';
  // Zip
  $row_data[27] = '8001';
  $row_data[28] = 'City';
  // ISO country code.
  $row_data[29] = 'CH';
}

/**
 * Alter the order product data.
 *
 * This hook is called for each single line item in the order.
 *
 * Make sure to add conditions for the line item type to prevent
 * general overrides.
 *
 * @param array $row_data
 * @param object $line_item
 * @param object $order
 */
function ja_job_lib_commerce_winbiz_product_data_alter(&$row_data, $line_item, $order) {
  if ($line_item->type == 'commerce_coupon') {
    // Load the product.
    $item = field_get_items('commerce_line_item', $line_item, 'commerce_coupon_reference');
    $coupon = entity_load_single('commerce_coupon', $item[0]['target_id']);

    $data[50] = $line_item->line_item_label;
    // 51 Product description.
    $data[51] = $data[76] = $data[77] = $data[78] = $data[79] = $data[80] = $line_item->line_item_label;
    $data[52] = strftime('%Y%m%d', $line_item->created);
    // 53 Quantity.
    $data[53] = 1;

    $item = field_get_items('commerce_line_item', $line_item, 'commerce_unit_price');
    $data[54] = commerce_currency_amount_to_decimal($item[0]['amount'], 'USD');
    // 55 Product unit.
    $line[55] = $line[85] = t('Piece');

    $coupon_percentage = field_get_items('commerce_coupon', $coupon, 'commerce_coupon_percent_amount');
    // 56 Discount.
    $line[56] = $coupon_percentage[0]['value'];
    // 57 Price.
    $data[57] = $data[83] = commerce_currency_amount_to_decimal($item[0]['amount'], 'USD');

    // 60 Sales turnover account number.
    $data[60] = variable_get('commerce_winbiz_profit_account_number', '3000');
    // 61 Tax %.
    $data[61] = commerce_winbiz_get_tax_rate($line_item);
    // Incl./excl. tax.
    $data[62] = variable_get('commerce_winbiz_tax_inclusion', 1);
    // 63 Tax account.
    $data[63] = variable_get('commerce_winbiz_vattax_account_number', '');
    // 64 Tax handling.
    $data[64] = 1;
    // 68 Tax type: 0, 1 =  no tax, 2 = due tax, 3 = pre-tax
    // 4 = outside of the field.
    $data[68] = 2;

    // Update of the article in WinBIZ upon import.
    $data[74] = 0;
    // Short description.
    $data[75] = $line_item->line_item_label;
  }
}
